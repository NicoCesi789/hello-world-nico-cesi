#Set the base image to node
FROM node:20-alpine as build

#Specify where our app will live in the container
WORKDIR /app

#Copy the react App to the container (only test, no prod)
COPY . /app/

#Prepare the container for building react
RUN npm install

#We want the production version
RUN npm run build

# Build le container en local 'docker build -t test:test -f docker/Dockerfile .'

# Prepare nginx image
FROM nginx:stable

#Copy complied app from previous container
COPY --from=build /app/build /usr/share/nginx/html

#Copy Nginx configuration
COPY docker/nginx.conf /etc/nginx/conf.d/default.conf

#Expose Nginx
EXPOSE 80

#Run Nginx
CMD ["nginx", "-g", "daemon off;"]



ENTRYPOINT [‘echo’, ‘hello’, ‘world’]
CMD [echo ‘hello world’]
